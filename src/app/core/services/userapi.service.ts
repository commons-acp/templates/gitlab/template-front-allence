import { Injectable } from '@angular/core';
import {User} from '../models/user.model';
import {HttpClient, HttpParams} from '@angular/common/http';
import {UserApi} from '../api/user.api';

@Injectable({
  providedIn: 'root'
})
export class UserapiService {



  constructor(private http: HttpClient , private  userApi : UserApi) {}

  getAllUsers(limit: any, page: any) {
  return this.userApi.getAllUsers(undefined,undefined)
  }

  ajoutUser(user: User) {
    return this.userApi.ajoutUser(user)
  }

  getUserById(id: any) {
  return this.userApi.getUserById(id);
  }

  updateUser(user: User) {
    return this.userApi.updateUser(user);

  }

  deleteUser(user: User) {
    return this.userApi.deleteUser(user);

  }

  getUserByUsername(username: string){
    return this.userApi.getUserByUsername(username);

  }

  getUserByEmail(email: string){
    return this.userApi.getUserByEmail(email);
  }
}


